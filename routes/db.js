var mysql = require('mysql');
var settings = {
    host: "db4free.net",
    user: "aakash",
    password: "aakash123",
    database: "niksms",
    multipleStatements: true

};
var db;

function connectDatabase() {
    if (!db) {
        db = mysql.createConnection(settings);
        console.log(__dirname);
        db.connect(function(err){
            if(!err) {
                console.log('Database is connected!');
            } else {
                console.log('Error connecting database!');
            }
        });
    }
    return db;
}

module.exports = connectDatabase();